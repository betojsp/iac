#!/bin/bash
apt-get -y update
apt-get -y install curl wget joe
# instalar docker
#curl -fsSL https://get.docker.com -o get-docker.sh
#sh get-docker.sh
apt-get -y install nginx
# Update index.html public ip
dig +short myip.opendns.com @resolver1.opendns.com >> /var/www/html/index.nginx-debian.html
