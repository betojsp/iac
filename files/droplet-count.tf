terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
    }
  }
}

variable "do_token" {}

provider "digitalocean" {
  token = var.do_token
}

locals {
  default_ttl = 1800
}

data "digitalocean_ssh_key" "tarsKey" {
  name = "id_rsa Beto"
}

resource "digitalocean_droplet" "dockersrv" {
  count              = 2
  image              = "ubuntu-20-04-x64"
  name               = "ds-${count.index}"
  region             = "nyc3"
  size               = "s-1vcpu-1gb"
  backups            = "true"
  monitoring         = "true"
  private_networking = "true"
  ssh_keys           = [data.digitalocean_ssh_key.tarsKey.id]
  user_data          = file("${path.module}/files/upgrade.sh")
}

output "server_ip" {
  value = digitalocean_droplet.dockersrv.0.ipv4_address
}

output "project_path" {
  value = abspath(path.root)
}

resource "digitalocean_record" "tarsReg" {
  domain = "tars.tools"
  type   = "A"
  name   = "www"
  value  = digitalocean_droplet.dockersrv.0.ipv4_address
  ttl    = local.default_ttl
}
