
terraform {
    required_providers {
        digitalocean = {
            source = "digitalocean/digitalocean"
        }
    }
}
 
variable do_token {}
provider digitalocean {
    token = var.do_token
}
 
resource "digitalocean_ssh_key" "default" {
  name       = "id_rsa Beto"
  public_key = file("/home/beto/.ssh/id_rsa.pub")
}
