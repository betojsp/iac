terraform {
    required_providers {
        digitalocean = {
            source = "digitalocean/digitalocean"
        }
    }
}
 
variable do_token {}
provider digitalocean {
    token = var.do_token
}
 
data "digitalocean_ssh_key" "tars" {
    name = "id_rsa Beto"
}
 
resource "digitalocean_droplet" "dockersrv" {
    image = "ubuntu-20-04-x64"
    name = "ds-1"
    region = "nyc3"
    size = "s-1vcpu-1gb"
    backups = "true"
    monitoring = "true"
    private_networking = "true"
    ssh_keys = [data.digitalocean_ssh_key.tars.id]
}
 
output "server_ip" {
    value = digitalocean_droplet.dockersrv.ipv4_address
}