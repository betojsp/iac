terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
    }
  }
}

variable "do_token" {}

provider "digitalocean" {
  token = var.do_token
}

locals {
  default_ttl = 1800
}

data "digitalocean_ssh_key" "tarsKey" {
  name = "id_rsa Beto"
}

resource "digitalocean_droplet" "dockersrv" {
  count              = 2
  image              = "ubuntu-20-04-x64"
  name               = "ds-${count.index}"
  region             = "nyc1"
  size               = "s-1vcpu-1gb"
  backups            = "true"
  monitoring         = "true"
  private_networking = "true"
  ssh_keys           = [data.digitalocean_ssh_key.tarsKey.id]
  user_data          = file("${path.module}/files/upgrade.sh")
}

output "server_ip" {
  value = digitalocean_droplet.dockersrv.0.ipv4_address
}

output "project_path" {
  value = abspath(path.root)
}

resource "digitalocean_certificate" "cert" {
  name    = "le-web-lb"
  type    = "lets_encrypt"
  domains = ["www.tars.tools"]
}

resource "digitalocean_loadbalancer" "web-lb" {
  name   = "lb-1"
  region = "nyc1"

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 80
    target_protocol = "http"

  }

  forwarding_rule {
    entry_port     = 443
    entry_protocol = "http2"

    target_port     = 80
    target_protocol = "http"

    certificate_name = digitalocean_certificate.cert.name
  }

  healthcheck {
    port     = 80
    protocol = "http"
    path     = "/"
  }

  redirect_http_to_https = true
  droplet_ids            = digitalocean_droplet.dockersrv.*.id
}

resource "digitalocean_record" "tarsReg" {
  domain = "tars.tools"
  type   = "A"
  name   = "www"
  value  = digitalocean_loadbalancer.web-lb.ip
  ttl    = local.default_ttl
}
